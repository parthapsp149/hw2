## Homework 2

**In this homework, we're going to explore files and filesystems**

- Just like last time, start by forking this repo into your own Gitlab.com account (via web), and then cloning that repo to your local machine (e.g.: `git clone git@gitlab.com:<your-username>/hw2.git`).

- I will ask questions as we go, and I expect you to answer them inside this file by using a text editor.
For example:

```markdown
#### [x pts] Fake Section:                      	(my text)
> Questions:               				    		(my text)
    - What kind of file are you reading?    		(my text)
        - It is a markdown file             		(your answer -- added with a text editor)
```

- I will also ask you to take a lot of screenshots.  I don't want/need your whole screen. Find a way to grab a small rectangular portion of your screen, so you can just capture what you need.  
	+ when I ask for screenshots, I will write something like:

	```markdown
	- Here is an instruction step
		+ screencap (capfile.png)
	```
	
	that means, take a screencap of the last step, and save it here in this repo as `capfile.png`.  If you want to get fancy, you can make a directory here called `screenshots` or something and keep all your captures inside.

- also, keep in mind that if one of the arguments in the command I give is a file, then you should be working in a directory that contains that file.  For example, when I run `cat mynewfile`, the `cat` program has to be able to find `mynewfile`. `cat` will not search your system for you.  You have to tell it exactly where that file is.  If I first`cd` to the directory which contains `mynewfile`, then I can just use the command as written.  Otherwise, I have to use `cat /full/path/to/mynewfile` so that `cat` can find it. So, if a command is operating on a file, you should check that either: A) the file is inside your current `$PWD` or B) you give the full path to the file, so the program you're calling can find it.

- several commands may require elevated permissions. If you get an error message something like "bad permissions" or "not readable," try using `sudo` in front of the same command

#### [3 pts] Inspect Megi's Multiboot Image for the Pinephone:

- read the instructions at https://xnux.eu/p-boot-demo/ 

- the files are hosted by Megi at http://dl.xnux.eu/p-boot-multi-2020-11-23/  and someone else is hosting a mirror at http://mirror.uxes.cz/dl.xnux.eu/p-boot-multi-2020-11-23/  (I've found the mirror to be faster).  I will be "seeding" the file via torrent, so maybe you can get it from me :)

- each of those directories contain three files:
  
  1. SHA256
  
  2. SHA256.asc
  
  3. multi.img.gz

- download all the files (you may use the .torrent link to get `multi.img.gz`)

- move your downloaded files into this `hw2` directory

> Questions:
>- What kind of file did you just download?
>> Ans: File type says, two are text files and one is zipped/archieved file.
Both text files seem containig some kind of cryptographic signature.
The zipped one containes the image of the distribution package.

>- Why do we call it an "image"?
>> Answer: Because this file is a complete snapshot. It includes the files of all partitions, partition table, and the file systems of each partition of a block device.
>- Why would Megi use this format to distribute it?
>> Answer: So that, they can provide all 17 distributions together. 

#### [3 pts] .gitignore:
**DO NOT ADD THESE NEW FILES TO THE REPOSITORY -- THEY ARE WAY TOO LARGE (and I already have them)**
  
- you can tell `git` to ignore certain files by making a list of things to ignore inside a new file called `.gitignore`  (remember that the `.` prefix means "hidden"?)
  
- edit the contents of the file with `nano .gitignore`. Make sure it looks like:
    
    ```
    (there may be other stuff here)
    SHA256
    SHA256.asc
    multi.img.gz
    *.img
    *.img.*
    ```
    
    
#### [3 pts] Hashes:

- use `cat SHA256` to see the contents of `SHA256`
    
	- save a screencap as "catsha.png"

- run the command `sha256sum multi.img.gz`
  
	- save a screencap as "shasum.png"

> Questions:
>   
>   - How do the output of the last two commands compare?
      Ans: sha256sum multi.img.gz shows the signature code corresponding to multi.img.gz
           which is the first line of SHA256.txt file
>   - Why would MEGI distribute the `SHA256` file with the image?  
     Ans: For verification purpose. So that, we can be sure it is the correct and uncorrupted image file.
	Maggi provided the result of his sha156sum on his image file so that we may do the same with ours and verify.


#### [3 pts] Look at the Multiboot image (download file):

- use the command `head` to show you the beginning of the file (contents)
	- screencap (headimggz.png)

> Questions:
> 	- Why does it look funny?  (use `head` to look at this .md file for comparison)
>> Answer: It is not a text file. Only ASCII characters in a text file are printable in command line.


#### [6 pts] Extract the actual image from what you downloaded

- use `zcat multi.img.gz > multi.img` to decompress the image file
 
- take a look at the head of the file with `head multi.img`
	+ screencap (headimg.png)

- take a look at the head in a hexdump format:
	+ `head multi.img | xxd`
	+ screencap (headimgxxd.png)
	
> ##### Questions:
> - what does "`zcat`" mean (look it up)? 
>> Ans: 'z' comes from some kind of zipped word. It actually unzip the file.

> - what happens after running this command? 
Ans: But in this case zact is use to unzip the image file
> - what do the contents of the new file look like as compared to the *.gz version?
>> The organization of the files are different.
#### [6 pts] Deeper inspection of image file

- run `sha256sum multi.img` on the new file
	+ screencap (shasumimg.png)

- use `file multi.img` to see if we can learn anything about the file
	+ screencap (filemulti.png)

- use `fdisk -l multi.img` to get more info about the file system on the image
	+ screencap (dumpemulti.png)

>Questions:
> - what do we know about the decompressed file `multi.img` ? 
>> 	Ans: Size= 9.78 GiB. I think we also know the location of the storage
> - what does it contain? 
>> 	Ans: It has two partitions. The first one (with a * mark is for the bootig and has no defined filesystem)
> - how did the output of the `sha256sum` command compare to the info in the `SHA256` file? 
	Ans: Shows the signature corresponding to multi.img, stored in SHA256

#### [6 pts] Make the image look like a block-device to the system

- run `losetup -f multi.img` to make the file look like a block-device (hard disk) to the system ( you may have to run this as a superuser by prefixing the command with`sudo`)

- run `losetup -a` to get a list of all "loopback" devices (what we just made)

- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device

- run `losetup -a` to confirm it's gone

- make it again :)

- `losetup -a` to see what our loop device number is (/dev/loop##) in case it changed

- find our file in the list
	+ screencap a couple lines of output including the one with our file (losetup.png)

- run `lsblk -f` to "list block-devices" with "full" output

- try `fdisk -l /dev/loop##` again on our new loopback device (replace ## with the correct number from above)

- try `file -s` again on the loopback device:
	+ `sudo file -s /dev/loop##`   (replace ## with the correct number from above)
	
- run `losetup -d /dev/loop##` (##==our number) to remove ("delete") our loopback device (again)

#### [6 pts] Recreate loopback device and scan

We are now going to re-create our loopback device again(!) but this time, we might add the argument `-P` to the `losetup` command.  Let's check the `losetup` manual to see what this will do:

+ `man losetup`  (the `man` command automatically uses `pager` which by default is `less` to view the manual.  use the arrows and `b` and `<space>` to move around and `<q>` to quit.)
	
	+ screencap portion of manual explaining `-P` parameter (manlosetup.png)
	
- Go ahead and do it:
	+ `losetup -fP multi.img`
	+ `losetup -a`

- run `lsblk -f` again, and screencap the portion of output with our newest loopback device


>Questions:
>	- did we get a different response from `file` or `fdisk` when using the virtual (loopback) block device?
>> Ans: yes different.

>	- is there anything suspicious about the output of either of those commands?
Ans: My naive eyes can not find any
>	- What happened after we ran `losetup -P ...`??  Why??
Ans: We know from the manual,  
losetup - set up and control loop devices
and 
-P, --partscan
		Force  the  kernel to scan the partition table on a newly created loop device.
		
		
#### [6 pts] Filesystem info (again)

Now that the block device (and any partitions) exist as virtual devices, let's look at the partitions individually:

- run `lsblk -af` to list all block devices.  Pay attention to our loopback device and make note of its partitions

- let's run `file -s /dev/loop##p#>` and `fdisk -l /dev/loop##p#` again on each partition to see if anything's changed.
	+ screencaps (filespart1.png, filespart2.png, fdiskpart1.png, fdiskpart2.png)


#### [10 pts] Mount the loopback devices

The main directory tree in Linux (the one we navigate with `cd` and `ls`) is a virtual filesystem.  It's the heart --and entirety-- of the whole system.  Any _real_ filesystems found on external drives, etc. have to be "mounted" into the main (virtual) system directory tree before they can be accessed or navigated.

- create a place to mount our devices:
	+ `sudo mkdir /mnt/pb1`
	+ `sudo mkdir /mnt/pb2`

- mount our loopback devices there:
	+ `sudo mount /dev/loop##p1 /mnt/pb1`
	+ `sudo mount /dev/loop##p2 /mnt/pb2`

- list the contents of the partitions:
	+ `ls /mnt/pb1`
	+ `ls /mnt/pb2`
	+ screencap (lspb1.png, lspb2.png)
	
>Questions:
>	- did these commands both succeed?
Ans: no
>	- If not, which one failed and why?
>> Ans: The first partition failes.
>	- what is the reason, do you think?
>> The first one is for the booting and has not file system. Mounting requires a defined file system.

#### [3 pts] Unmount the loopback devices

After adding external filesystems into the main (virtual) system FS, we might want to remove it.

- See a list of all mounted filesystems with `mount`
- Unmount the loopback devices from the system fs with `sudo umount /dev/loop##p#` (yes, it's "umount" and not "unmount")

> Questions:
	- where does the _real_ file system on the loopback device reside?
>> Here: /mnt/pb2/. Here,  '/' at the beginning indicates root. Inside the file system from teh VM desktop we can browse to this location and may find all the distribition files.

	- what happens to it after we unmount the device?

>> The folder, /mnt/pb2/ becomes empty.
